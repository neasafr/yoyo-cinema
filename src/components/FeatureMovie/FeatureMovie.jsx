﻿import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
// Styles
import './FeatureMovie.css';
// Helpers
import months from '../../helpers/date';

class FeatureMovie extends React.Component {

  render() {
    const { data: 
    {
      title,
      backdrop_path,
      release_date,
      overview,
    } } = this.props;

    const image = backdrop_path && 'https://image.tmdb.org/t/p/w1400_and_h450_face' + backdrop_path;
    var splitDate = release_date && release_date.split("-");

    const div = backdrop_path ? (
      <div className="feature-movie" style={{backgroundImage: 'url(' + image + ')'}}>
        <div className="movie-info">
          <p className="sub-text no-margin">UPCOMING MOVIES</p>
          <h2>{title}</h2>
          {splitDate && (<p className="sub-text release-date">{`${splitDate[2]}  ${months[splitDate[1]]} ${splitDate[0]}`}</p>)}
          <p>{overview}</p>
          <p className="sub-text upcoming-link">
            <Link to={`/upcoming`}>
                <strong>See More Upcoming Movies &rarr;</strong>
             </Link>
           </p>
        </div>
        <div className="overlay" />
      </div>) : <div />;

    return div;
  }
}

FeatureMovie.propTypes = {
  data: PropTypes.object,
};

FeatureMovie.defaultProps = {
  data: 
    {
      title: 'Aquaman',
      backdrop_path: '/5A2bMlLfJrAfX9bqAibOL2gCruF.jpg',
      vote_average: 6.9,
      release_date: '2018-12-07',
      overview: 'Arthur Curry learns that he is the heir to the underwater kingdom of Atlantis, and must step forward to lead his people and be a hero to the world.',
    },
};

export default FeatureMovie;
