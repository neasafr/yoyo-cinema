﻿import React, { Component } from 'react';
import { Route, withRouter, Switch } from 'react-router-dom';
// Components
import NavBar from './components/NavBar/NavBar';
import SearchBar from './components/SearchBar/SearchBar';
import Home from './pages/Home/Home';
import SingleMovie from './pages/SingleMovie/SingleMovie';
import Favourites from './pages/Favourites/Favourites';
import Upcoming from './pages/Upcoming/Upcoming';
import ThisMonth from './pages/ThisMonth/ThisMonth';
import Search from './pages/Search/Search';
// how to import image
// import logo from './logo.svg';
//Glbale css import
import './style/global.css';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = { showSearch: false };
    this.toggleSearch = this.toggleSearch.bind(this);
  }

  toggleSearch() {
    const { showSearch } = this.state;
    this.setState({ showSearch: !showSearch });
  }

  render() {
    const { showSearch } = this.state;
    const { location: { pathname} } = this.props;

    return (
      <div className="App">
        <NavBar toggleSearch={this.toggleSearch} dark={pathname.indexOf('search') >= 0 || pathname.indexOf('favourites') >= 0} />
        {showSearch && <SearchBar onClick={this.toggleSearch} />}
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/movies/:movieSlug" component={SingleMovie} />
          <Route path="/favourites" component={Favourites} />
          <Route path="/upcoming" component={Upcoming} />
          <Route path="/this-month" component={ThisMonth} />
          <Route path="/search/:searchTerm" component={Search} />
        </Switch>
      </div>
    );
  }
}

export default withRouter(App);
