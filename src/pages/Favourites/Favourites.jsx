﻿import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
// Actions
import { fetchFavMovies } from "../../redux/actions/favourites";
// Components
import MoviePosterList from '../../components/MoviePosterList/MoviePosterList';
// Styles
import './Favourites.css';

class Favourites extends Component {
  componentDidMount() {
    this.props.dispatch(fetchFavMovies());
  }

  render() {
    const { favourites } = this.props;

    return (
      <div className="favourites">
        <MoviePosterList heading="My Favourites" data={favourites} />
      </div>
    );
  }
}

Favourites.propTypes = {
  favourites: PropTypes.array,
};

Favourites.defaultProps = {
  favourites: [],
};

const mapStateToProps = state => ({
  favourites: state.favourites.data,
});

export default connect(mapStateToProps)(Favourites);
