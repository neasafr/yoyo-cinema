﻿import * as types from '../types';

const initialState = {
  data: [],
  loading: false,
  error: null,
};

export default function(state = initialState, { type, payload }) {
  switch (type) {
    case types.FETCH_MOVIES_BEGIN:
      return { ...state, loading: true, error: null };
    case types.FETCH_MOVIES_SUCCESS:
      return { ...state, loading: false, data: payload.movies };
    case types.FETCH_UPCOMING_MOVIES_SUCCESS:
      return { ...state, loading: false, upcoming: payload.movies };
    case types.FETCH_MOVIES_FAILURE:
      return { ...state, loading: false, error: payload.error, data: [] };
    default:
      return state;
  }
}
