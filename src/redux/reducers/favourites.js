﻿import * as types from '../types';

const initialState = {
  data: [],
};

export default function(state = initialState, { type, payload }) {
  switch (type) {
    case types.FETCH_FAV_SUCCESS:
    console.log('success');
      return { ...state, data: payload.movies };
    case types.ADD_FAV_SUCCESS:
        const { data } = state;
        console.log('adding');
        data.push(payload.newFav);
        if (localStorage.getItem('favState') !== data) {
            localStorage.setItem('favState', data);
            console.log(data);
        }
        return {...state, data };
    default:
      return state;
  }
}
