import * as types from '../types';

const initialState = {
  data: [],
  loading: false,
  error: null,
};

export default function(state = initialState, { type, payload }) {
  switch (type) {
    case types.FETCH_SEARCH_BEGIN:
      return { ...state, loading: true, error: null };
    case types.FETCH_SEARCH_SUCCESS:
      console.log('searching success');
      return { ...state, loading: false, data: payload.search };
    case types.FETCH_SEARCH_FAILURE:
      return { ...state, loading: false, error: payload.error, data: [] };
    default:
      return state;
  }
}
