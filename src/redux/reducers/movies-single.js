import * as types from '../types';

const initialState = {
  data: [],
  loading: false,
  error: null,
  videos: [],
  credits: [],
};

export default function(state = initialState, { type, payload }) {
  switch (type) {
    case types.FETCH_MOVIES_SINGLE_BEGIN:
      return { ...state, loading: true, error: null };
    case types.FETCH_MOVIES_SINGLE_SUCCESS:
      return { ...state, loading: false, data: payload.movies };
    case types.FETCH_MOVIES_SINGLE_VIDEO_SUCCESS:
      return { ...state, videos: payload.videos };
    case types.FETCH_MOVIES_SINGLE_CAST_SUCCESS:
    console.log(payload);
      return { ...state, credits: payload.credits };
    case types.FETCH_MOVIES_SINGLE_FAILURE:
      return { ...state, loading: false, error: payload.error, data: [] };
    default:
      return state;
  }
}
