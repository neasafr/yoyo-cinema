import { combineReducers } from "redux";
import movies from "./reducers/movies";
import moviesSingles from "./reducers/movies-single";
import favourites from "./reducers/favourites";
import search from "./reducers/search";

export default combineReducers({
  movies,
  moviesSingles,
  favourites,
  search,
});